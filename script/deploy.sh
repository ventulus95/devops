echo "#### Build 파일 확인"

echo "#### 현재 구동중인 애플리케이션 PID 확인"

CURRENT_PID=$(lsof -i :8080 | grep LISTEN | awk '{print $2}')

if [ -z "$CURRENT_PID" ]; then
  echo "구동중인거 없음! 다시 진행하겠습니다."
else
  echo "구동중인 서버 존재! 종료"
  kill -15 $CURRENT_PID
  echo "종료되었습니다."
  sleep 2
fi

echo "## 새로운 애플리케이션 배포~~~"

JAR_NAME=$(ls -tr ~/Desktop/jars/*.jar | tail -n 1)

echo "이 파일명을 실행! >> $JAR_NAME"

chmod +x $JAR_NAME

nohup java -jar $JAR_NAME > `pwd`/nohup.out 2>&1 &

echo "뭐가 되었든 일단 실행되었음!"